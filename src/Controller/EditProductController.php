<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\EditProduct;
use App\Form\EditProductType;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;

class EditProductController extends Controller
{
    /**
     * @Route("/edit/product", name="edit_product")
     */
    public function index(Request $request, FileUploader $fileUploader)
    {
        $editProduct = new EditProduct();
        $form = $this->createForm(EditProductType::class, $editProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $editProduct->getFile();
            $fileName = $fileUploader->upload($file);

            return $this->render('edit_product/show.html.twig', [
                'fileURI' => $this->getParameter('files_URI') . $fileName
            ]);

        }
        return $this->render('edit_product/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
