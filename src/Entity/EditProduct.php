<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class EditProduct
{
    private $id;
    /**
     *@Assert\NotBlank(message="Please, upload the product image as .jpg, .jpeg or .png file.") 
     * @Assert\File(mimeTypes={ "image/jpeg", "image/png" }) 
     */
    private $file;

    public function getId()
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file) : self
    {
        $this->file = $file;

        return $this;
    }
}

